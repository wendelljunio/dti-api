﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using dtiapi.Data;
using dtiapi.Models;
using dtiapi.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace dti_api.Controllers
{
    [Route("v1/products")]
    public class ProductController : Controller
    {
        [HttpGet]
        [Route("")]
        public async Task<ActionResult<List<Product>>> ListProducts([FromServices]ProductRepository productRepository)
        {
            return Ok(await productRepository.ListProducts());
        }

        [HttpGet]
        [Route("{id:int}")]
        public async Task<ActionResult<Product>> GetById(int id, [FromServices]ProductRepository productRepository)
        {
            return Ok(await productRepository.GetById(id));
        }

        [HttpPost]
        [Route("")]
        public async Task<ActionResult<List<Product>>> CreateProduct(
            [FromBody]Product product,
            [FromServices]ProductRepository productRepository
        )
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            try
            {
                return Ok(await productRepository.CreateProduct(product));
            }
            catch
            {
                return BadRequest(new { message = "Não foi possivel criar o produto. Tente novamente!" });
            }

        }

        [HttpPut]
        [Route("{id:int}")]
        public async Task<ActionResult<List<Product>>> EditProduct(
            int id,
            [FromBody]Product product,
            [FromServices]ProductRepository productRepository
        )
        {
            if (id != product.Id)
                return NotFound(new { message = "Produto não encontrado" });

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            try
            {
                return Ok(await productRepository.EditProduct(product));
            }
            catch (DbUpdateConcurrencyException)
            {
                return BadRequest(new { message = "Não foi possivel atualizar o produto. Tente novamente!" });
            }
            catch (Exception)
            {
                return BadRequest(new { message = "Ocorreu um erro. Tente novamente!" });
            }
        }

        [HttpDelete]
        [Route("{id:int}")]
        public async Task<ActionResult<List<Product>>> Delete(int id, [FromServices]ProductRepository productRepository)
        {
            var product = await productRepository.GetById(id);
            if (product == null)
                return NotFound(new { message = "Produto não encontrado!" });

            try
            {
                return Ok(await productRepository.Delete(id));
            }
            catch (Exception)
            {
                return BadRequest(new { message = "Não foi possivel remover o produto" });
            }
        }
    }
}
