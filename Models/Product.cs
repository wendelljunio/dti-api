﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace dtiapi.Models
{
    public class Product
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "O campo nome é obrigatorio")]
        [MaxLength(80, ErrorMessage = "O maximo de caracteres é 80")]
        [MinLength(2, ErrorMessage = "O minimo de caracteres é 2")]
        public string Name { get; set; }

        [Required(ErrorMessage = "O campo quantidade é obrigatorio")]
        public int Quantity { get; set; }

        [Required(ErrorMessage = "O campo preço é obrigatorio")]
        [Range(1, int.MaxValue, ErrorMessage = "O preço deve ser maior que zero")]
        [Column(TypeName = "decimal(18, 2)")]
        public decimal Price { get; set; }

    }
}
