﻿using dtiapi.Models;
using Microsoft.EntityFrameworkCore;

namespace dtiapi.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        { }

        public DbSet<Product> Products { get; set; }
    }
}
